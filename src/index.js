import './main.css'
import { createStore, applyMiddleware } from 'redux';
import React from 'react';
import ReactDOM from 'react-dom';
import thunk from 'redux-thunk';
import { browserHistory, Router, Route } from 'react-router';
import { syncHistoryWithStore } from 'react-router-redux';
import { Provider } from 'react-redux';
import { composeWithDevTools } from 'redux-devtools-extension';
import reducers from 'reducers';

const store = createStore(reducers, composeWithDevTools(applyMiddleware(thunk)));

const history = syncHistoryWithStore(browserHistory, store);
import Layout from 'containers/layout';
import Phones from 'containers/phones';

ReactDOM.render(
    <Provider store={ store }>
        <Router history={ history }>
          <Route component={ Layout }>
            <Route path="/" component={ Phones }></Route>
          </Route>
        </Router>
    </Provider>,
    document.getElementById('root')
);